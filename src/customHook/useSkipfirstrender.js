let flag = false;
function useSkipfirstrender(func, state) {
  if (!flag) {
    flag = true;
    return;
  }
  func();
}

export default useSkipfirstrender;

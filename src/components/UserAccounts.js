import React from "react";
import Card from "../utils/Card";
import PropTypes from "prop-types";
import { useDispatch, useSelector } from "react-redux";

import { fetchUsers } from "../actions";

function UserAccounts(props) {
  const [search, setSearch] = React.useState("");
  const [userAcc, setUserAcc] = React.useState([]);
  const dispatch = useDispatch();
  const users = useSelector((store) => store.users);

  React.useEffect(() => {
    const timer = setTimeout(() => {
      setUserAcc([
        ...users.filter((ele) => {
          const len = search.length;
          if (
            search.toLocaleLowerCase() ===
            ele.name.slice(0, len).toLocaleLowerCase()
          )
            return ele;
          return "";
        }),
      ]);
    }, 1000);

    return () => {
      clearTimeout(timer);
    };
  }, [search, users]);

  React.useEffect(() => {
    dispatch(fetchUsers());
  }, []);

  const setUserSearch = (eve) => {
    setSearch(eve.target.value);
  };

  return (
    <div data-test="userAccount-component">
      <input
        placeholder="search by names"
        className="search"
        type="text"
        onChange={(eve) => setUserSearch(eve)}
      />
      <Card list={userAcc} />
    </div>
  );
}

UserAccounts.propTypes = {};

export default UserAccounts;

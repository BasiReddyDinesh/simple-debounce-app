import React from "react";
import { useParams } from "react-router-dom";
import PropTypes from "prop-types";
import { useSelector } from "react-redux";

function UserDetails(props) {
  const { id } = useParams();
  const user = useSelector((store) => ({ ...store.users[Number(id) - 1] }));
  console.log(user);
  return (
    <div data-test="userDetails-component">
      <div className="user_container">
        <div className="item">{user.id}</div>
        <div className="item">{user.name}</div>
        <div className="item">{user.email}</div>
        <div className="item">{user.company.name}</div>
        <div className="item">{user.phone}</div>
        <div className="item">{user.username}</div>
        <div className="item">{user.website}</div>
      </div>
    </div>
  );
}

UserDetails.propTypes = {};

export default UserDetails;
